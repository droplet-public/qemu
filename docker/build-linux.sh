set -euo pipefail

cd "$(dirname "$0")"

docker build -t qemu .

CONFIGURE_OPTIONS='--target-list=x86_64-softmmu --static --disable-debug-info --disable-stack-protector --disable-capstone --enable-vnc-png --disable-vnc-sasl --disable-qom-cast-debug --enable-trace-backends=log --disable-smartcard --disable-guest-agent --disable-tools --disable-libxml2 --enable-slirp=git --disable-libusb --disable-vhost-net --disable-vhost-vsock --disable-vhost-scsi --disable-vhost-crypto --disable-vhost-kernel --disable-vhost-user --disable-sdl --disable-gtk --disable-curses'

docker run --volume=$PWD/..:/qemu/ -it qemu /bin/bash -c "cd /qemu/ && rm -rf build-linux && mkdir build-linux && cd build-linux && ../configure $CONFIGURE_OPTIONS && make"

# To continue compilation if it failed:
#docker run --volume=$PWD/..:/qemu/ -it qemu /bin/bash -c 'cd /qemu/ && make'
