set -euo pipefail
cd "$(dirname "$0")"

# install dependencies: brew install libtool pkg-config glib pixman libpng dylibbundler
# https://formulae.brew.sh/formula/qemu
# https://github.com/auriamg/macdylibbundler

# Note: Removed --static
CONFIGURE_OPTIONS='--target-list=x86_64-softmmu --disable-debug-info --disable-stack-protector --disable-capstone --enable-vnc-png --disable-vnc-sasl --disable-qom-cast-debug --enable-trace-backends=log --disable-smartcard --disable-guest-agent --disable-tools --disable-libxml2 --enable-slirp=git --disable-libusb --disable-vhost-net --disable-vhost-vsock --disable-vhost-scsi --disable-vhost-crypto --disable-vhost-kernel --disable-vhost-user --disable-sdl --disable-gtk --disable-curses'
MAC_CONFIGURE_OPTIONS="$CONFIGURE_OPTIONS --enable-hvf --enable-hax"

rsync -avz --progress --delete --exclude build-linux --exclude build-windows .. mac:qemu-docker-static

# Run: ssh mac
# And then: qemu-docker-static/docker/build-mac-host.sh
#
# And copy out qemu-system-x86_64 and lib
