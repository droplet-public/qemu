set -euo pipefail
cd "$(dirname "$0")"

# This file should be run on a Mac machine

CONFIGURE_OPTIONS='--target-list=x86_64-softmmu --disable-debug-info --disable-stack-protector --disable-capstone --enable-vnc-png --disable-vnc-sasl --disable-qom-cast-debug --enable-trace-backends=log --disable-smartcard --disable-guest-agent --disable-tools --disable-libxml2 --enable-slirp=git --disable-libusb --disable-vhost-net --disable-vhost-vsock --disable-vhost-scsi --disable-vhost-crypto --disable-vhost-kernel --disable-vhost-user --disable-sdl --disable-gtk --disable-curses'
MAC_CONFIGURE_OPTIONS="$CONFIGURE_OPTIONS --enable-hvf --enable-hax"

cd ..
mkdir build
cd build
../configure $MAC_CONFIGURE_OPTIONS
make
cd x86_64-softmmu
dylibbundler --overwrite-dir --bundle-deps --fix-file qemu-system-x86_64 --install-path @executable_path/lib/ --dest-dir lib
